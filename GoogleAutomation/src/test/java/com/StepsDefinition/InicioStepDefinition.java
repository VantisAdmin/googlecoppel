package com.StepsDefinition;

import com.Pages.InicioPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class InicioStepDefinition {

	@Steps
	InicioPage _InicioPage;
	
	@Given("^Buscar palabra en el buscador$")
	public void Buscar_palabra_en_el_buscador() throws Exception {
		try {
			
			_InicioPage.Buscar_Palabra();
		} catch (Exception ex) {
			System.out.println(ex);
		}
	} 
	
	@Then("^Dar click a buscar$")
	public void Dar_click_a_buscar() throws Exception {
		try {
			
			_InicioPage.Dar_clic();
		} catch (Exception ex) {
			System.out.println(ex);
		}
	} 
	
	
	
}
