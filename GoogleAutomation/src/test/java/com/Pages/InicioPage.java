package com.Pages;

import com.Utils.Util;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;

public class InicioPage extends PageObject {
	Util _Util;

	@FindBy(name = "q")
	private WebElementFacade txtSearch;
	
	@FindBy(name = "btnK")
	private WebElementFacade btnOk;
	

	@Step
	public void Buscar_Palabra() {
		txtSearch.sendKeys("Coopel");
		_Util.ThreadSleep(2);

	}

	@Step
	public void Dar_clic() {
		btnOk.click();
		_Util.ThreadSleep(2);
	}
}
