package com.Utils;

import net.serenitybdd.core.pages.PageObject;

public class Util extends PageObject {

	// Metodos y logica
	public void ThreadSleep(int segundos) {
		try {
			Thread.sleep(segundos * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
